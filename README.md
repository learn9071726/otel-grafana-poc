# otel-grafana

This project serves as a POC for implementing application observability using the OpenTelemetry in conjunction with the Grafana stack. 

This contains two backend services. One written in SpringBoot and one in GoLang. 
Both the services are exposing some example endpoints.


![Alt text](image.png)


## How to spin up the project locally ?

You need to have docker installed in your machiene. 

There is a `docker-compose.yaml` file at the rool level of the repository.

cd in to project root and execute bellow command
```
docker compose up
```

That command alone is enough to start the whole tool set together with the services of which we will be observing using otel + grafana.

## Invoking endpoints invoked by services

Go service exposes following endpoint.
```
curl --location 'http://localhost:8080/hello-from-go'
```

SpringBoot service exposes following endpoints.
```
curl --location 'http://localhost:9094/otel-tracing/hi'
```

```
curl --location 'http://localhost:9094/otel-tracing/sleep/1'
```

```
curl --location 'http://localhost:9094/otel-tracing/sleep/2'
```

```
curl --location 'http://localhost:9094/otel-tracing/sleep/3'
```

```
curl --location 'http://localhost:9094/otel-tracing/sleep/4'
```

```
curl --location 'http://localhost:9094/otel-tracing/sleep/5'
```

```
curl --location 'http://localhost:9094/otel-tracing/hi-go'
```

The last endpoint(that is expopsed from the spring service) calls go service internally. We can use that to visualize service to service calls in Grafana.

## How to check Grafana dashboard.

Head over to localhost:3000 to log into Grafana. Use following credentials to login. 

```
username: admin
password: admin
```

you can skip the password reset prompt in the next screen.

## Building a dashboard using exported metrices.

We have already setup two data sources for grafana using the docker-compose file. (Prometheus and Tempo)

Create a new dashboard and select the option to add a new visualization to it.
In the next window, you can pick a datasource from available ones and add required filters to it to get a visualization added in to the dashboard.  

