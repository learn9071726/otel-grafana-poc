package main

import (
	"context"
	"go-tracing/controller"
	"go-tracing/logging"
	"go-tracing/otel"
	"go-tracing/service"
)

func main() {
	serviceName := "otel-go-vg-tracing"
	ctx := context.Background()

	// Init tracing
	tp, err := otel.InitTraceProvider(ctx, serviceName)
	if err != nil {
		panic(err)
	}
	defer tp.Shutdown(ctx)

	// Init metrics
	mp, err := otel.InitMetricProvider(ctx, serviceName)
	if err != nil {
		panic(err)
	}
	defer mp.Shutdown(ctx)

	logger := logging.InitLogger()

	service.InitHelloService()
	controller.InitHelloController()

	InitRoutes(ctx, logger)
}
