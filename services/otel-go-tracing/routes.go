package main

import (
	"context"
	"github.com/go-chi/chi"
	"github.com/sirupsen/logrus"
	"go-tracing/controller"
	"go-tracing/logging"
	mw "go-tracing/middleware"
	"net/http"
)

func InitRoutes(ctx context.Context, logger *logrus.Logger) {

	ctrl := controller.GetHelloController()

	r := chi.NewRouter()
	r.Use(mw.OtelMonitoring)
	//r.Use(middleware.RequestID)
	//r.Use(middleware.Logger)

	r.Group(func(r chi.Router) {
		r.Use(logging.NewStructuredLogger(logger))
		r.Route("/hello-from-go", func(r chi.Router) {
			r.Get("/", ctrl.SayHello)
		})
	})

	//s := service.HelloService{}
	//r.Get("/hello-from-go", func(writer http.ResponseWriter, request *http.Request) {
	//
	//	msg := s.SayHello(ctx)
	//	data, _ := json.Marshal(msg)
	//	_, err := writer.Write(data)
	//	if err != nil {
	//		return
	//	}
	//	writer.WriteHeader(200)
	//	return
	//})

	http.ListenAndServe(":8080", r)

}
