package logging

import (
	"context"
	"github.com/go-chi/chi/middleware"
	"github.com/sirupsen/logrus"
	"os"
)

var Logger = logrus.New()

func InitLogger() *logrus.Logger {

	logrusLogger := logrus.New()

	logrusLogger.Level = logrus.InfoLevel
	logrusLogger.Formatter = new(logrus.JSONFormatter)
	logrusLogger.Out = os.Stdout

	logrusLogger.Info("Initialized Logger successfully with 'Stdout' output.")

	return logrusLogger

}

func GetLogger(ctx context.Context) *StructuredLoggerEntry {
	logger := ctx.Value(middleware.LogEntryCtxKey)
	if logger != nil {
		logEntry := logger.(*StructuredLoggerEntry)
		//logEntry.Logger = logEntry.Logger.WithField("@timestamp", time.Now().Format(time.RFC3339Nano))
		return logEntry
	}
	return &StructuredLoggerEntry{Logger: Logger}
}
