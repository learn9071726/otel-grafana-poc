package logging

import (
	"fmt"
	"github.com/go-chi/chi/middleware"
	"github.com/sirupsen/logrus"
	"go.opentelemetry.io/otel/trace"
	"net/http"
)

type StructuredLogger struct {
	Logger *logrus.Logger
}

func NewStructuredLogger(logger *logrus.Logger) func(next http.Handler) http.Handler {
	return middleware.RequestLogger(&StructuredLogger{logger})
}

func (s StructuredLogger) NewLogEntry(r *http.Request) middleware.LogEntry {

	entry := &StructuredLoggerEntry{
		Logger: logrus.NewEntry(s.Logger),
	}
	logFields := logrus.Fields{}

	//logFields["@timestamp"] = time.Now().Format(time.RFC3339Nano)

	if reqID := middleware.GetReqID(r.Context()); reqID != "" {
		logFields["req_id"] = reqID
	}

	span := trace.SpanFromContext(r.Context())
	traceId := span.SpanContext().TraceID().String()
	spanId := span.SpanContext().SpanID().String()
	if spanId != "" {
		logFields["span_id"] = spanId
	}
	if traceId != "" {
		logFields["trace_id"] = traceId
	}

	entry.Logger = entry.Logger.WithFields(logFields)

	scheme := "http"
	if r.TLS != nil {
		scheme = "https"
	}
	logFields["http_scheme"] = scheme
	logFields["http_proto"] = r.Proto
	logFields["http_method"] = r.Method

	logFields["remote_addr"] = r.RemoteAddr
	logFields["user_agent"] = r.UserAgent()

	logFields["uri"] = fmt.Sprintf("%s://%s%s", scheme, r.Host, r.RequestURI)

	logger := entry.Logger.WithFields(logFields)

	logger.Infoln("request started")

	return entry

}
