package logging

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"net/http"
	"time"
)

type StructuredLoggerEntry struct {
	Logger logrus.FieldLogger
}

func (l *StructuredLoggerEntry) Write(status, bytes int, headers http.Header, elapsed time.Duration, val interface{}) {
	l.Logger = l.Logger.WithFields(logrus.Fields{
		"resp_status": status, "resp_bytes_length": bytes,
		"resp_elapsed_ms": float64(elapsed.Nanoseconds()) / 1000000.0,
	})

	l.Logger.Infoln("request complete")
}

func (l *StructuredLoggerEntry) Panic(v interface{}, stack []byte) {
	l.Logger = l.Logger.WithFields(logrus.Fields{
		"stack": string(stack),
		"panic": fmt.Sprintf("%+v", v),
	})
}

func (logger *StructuredLoggerEntry) Error(err error, args ...interface{}) {
	logger.Logger.WithError(err).Errorln(args...)
}

func (logger *StructuredLoggerEntry) Errorf(err error, fmt string, args ...interface{}) {
	logger.Logger.WithError(err).Errorf(fmt, args...)
}

func (logger *StructuredLoggerEntry) Warn(args ...interface{}) {
	logger.Logger.Warnln(args...)
}

func (logger *StructuredLoggerEntry) Warnf(fmt string, args ...interface{}) {
	logger.Logger.Warnf(fmt, args)
}

func (logger *StructuredLoggerEntry) Info(args ...interface{}) {
	logger.Logger.Infoln(args...)
}

func (logger *StructuredLoggerEntry) Infof(fmt string, args ...interface{}) {
	logger.Logger.Infof(fmt, args...)
}

func (logger *StructuredLoggerEntry) Debug(args ...interface{}) {
	logger.Logger.Debugln(args...)
}

func (logger *StructuredLoggerEntry) Debugf(fmt string, args ...interface{}) {
	logger.Logger.Debugf(fmt, args...)
}
