package otel

import (
	"context"
	"fmt"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/exporters/otlp/otlpmetric/otlpmetricgrpc"
	sdkmetric "go.opentelemetry.io/otel/sdk/metric"
	"go.opentelemetry.io/otel/sdk/resource"
	semconv "go.opentelemetry.io/otel/semconv/v1.12.0"
	"time"
)

func InitMetricProvider(ctx context.Context, serviceName string) (*sdkmetric.MeterProvider, error) {

	exp, err := initMetricExporter(ctx)
	if err != nil {
		return nil, fmt.Errorf("failed to initialize Metric exporter: %v", err)
	}

	// labels/tags/resources that are common to all metrics.
	res := resource.NewWithAttributes(
		semconv.SchemaURL,
		semconv.ServiceNameKey.String(serviceName),
		attribute.String("some-attribute", "some-value"),
	)

	mp := sdkmetric.NewMeterProvider(
		sdkmetric.WithResource(res),
		sdkmetric.WithReader(
			// collects and exports metric data every 30 seconds.
			sdkmetric.NewPeriodicReader(exp, sdkmetric.WithInterval(30*time.Second)),
		),
	)
	otel.SetMeterProvider(mp)
	fmt.Println("Otel collector is successfully initiated for metrics")
	return mp, nil
}

func initMetricExporter(ctx context.Context) (sdkmetric.Exporter, error) {
	secureOption := otlpmetricgrpc.WithInsecure()
	exporter, err := otlpmetricgrpc.New(
		ctx,
		otlpmetricgrpc.WithEndpoint("collector:5555"),
		secureOption,
	)
	if err != nil {
		return nil, fmt.Errorf("failed to create oltp metric exporter: %w", err)
	}

	return exporter, nil
}
