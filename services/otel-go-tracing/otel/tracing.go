package otel

import (
	"context"
	"fmt"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/exporters/otlp/otlptrace"
	"go.opentelemetry.io/otel/exporters/otlp/otlptrace/otlptracegrpc"
	"go.opentelemetry.io/otel/propagation"
	"go.opentelemetry.io/otel/sdk/resource"
	"go.opentelemetry.io/otel/sdk/trace"
	semconv "go.opentelemetry.io/otel/semconv/v1.17.0"
)

func InitTraceProvider(ctx context.Context, serviceName string) (*trace.TracerProvider, error) {

	// For tracing
	tp, err := newTraceProvider(ctx, serviceName)
	if err != nil {
		return nil, err
	}
	otel.SetTracerProvider(tp)
	otel.SetTextMapPropagator(propagation.NewCompositeTextMapPropagator(
		propagation.TraceContext{},
		propagation.Baggage{}))
	fmt.Println("Otel collector is successfully initiated for tracing")
	return tp, nil
}

func newTraceProvider(ctx context.Context, serviceName string) (*trace.TracerProvider, error) {
	exp, err := newExporter(ctx)
	if err != nil {
		return nil, err //fmt.Errorf("failed to initialize exporter: %v", err)
	}
	// Ensure default SDK resources and the required service name are set.
	r, err := resource.Merge(
		resource.Default(),
		resource.NewWithAttributes(
			semconv.SchemaURL,
			semconv.ServiceName(serviceName),
		),
	)
	if err != nil {
		return nil, err
	}
	return trace.NewTracerProvider(
		trace.WithSampler(trace.AlwaysSample()),
		trace.WithBatcher(exp),
		trace.WithResource(r),
	), nil
}

func newExporter(ctx context.Context) (*otlptrace.Exporter, error) {
	secureOption := otlptracegrpc.WithInsecure()
	exporter, err := otlptrace.New(
		ctx,
		otlptracegrpc.NewClient(
			secureOption,
			otlptracegrpc.WithEndpoint("collector:5555"),
		),
	)
	if err != nil {
		return nil, fmt.Errorf("failed to create oltptrace exporter: %w", err)
	}
	return exporter, nil
}
