package controller

import (
	"encoding/json"
	"go-tracing/logging"
	"go-tracing/service"
	"net/http"
)

type IHelloController interface {
	SayHello(writer http.ResponseWriter, request *http.Request)
}

var helloControllerInstance IHelloController

func GetHelloController() IHelloController {
	return helloControllerInstance
}

type helloController struct {
}

func InitHelloController() {
	helloControllerInstance = &helloController{}
}

func (h helloController) SayHello(writer http.ResponseWriter, request *http.Request) {
	ctx := request.Context()

	logger := logging.GetLogger(ctx)

	res := service.GetHelloService().SayHello(ctx)

	logger.Infof("[helloController :: SayHello] Greeting from service %s", res)

	data, _ := json.Marshal(res)
	_, err := writer.Write(data)
	if err != nil {
		return
	}
	writer.WriteHeader(200)
	return
}
