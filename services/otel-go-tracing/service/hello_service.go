package service

import (
	"context"
	"go-tracing/logging"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/trace"
)

var helloServiceInstance IHelloService

type IHelloService interface {
	SayHello(ctx context.Context) string
}

type helloService struct {
}

func InitHelloService() {
	helloServiceInstance = &helloService{}
}

func GetHelloService() IHelloService {
	return helloServiceInstance
}

func (receiver helloService) SayHello(ctx context.Context) string {

	ctx, span := otel.Tracer("myTracer").Start(ctx, "HelloService_SayHello")
	defer span.End()

	logger := logging.GetLogger(ctx)

	name := receiver.GetName(ctx)

	logger.Infof("[helloService :: SayHello] Received name %s", name)

	msg := "Hello " + name
	return msg
}

func (receiver helloService) GetName(ctx context.Context) string {

	ctx, span := otel.Tracer("myTracer").Start(
		ctx,
		"HelloService_GetName",
		// add labels/tags/resources(if any) that are specific to this scope.
		trace.WithAttributes(attribute.String("component", "addition")),
		trace.WithAttributes(attribute.String("someKey", "someValue")),
	)
	defer span.End()

	//counter, _ := global.MeterProvider().
	//	Meter(
	//		"instrumentation/package/name",
	//		metric.WithInstrumentationVersion("0.0.1"),
	//	).
	//	Int64Counter(
	//		"add_counter",
	//		instrument.WithDescription("how many times add function has been called."),
	//	)
	//counter.Add(
	//	ctx,
	//	1,
	//	// labels/tags
	//	attribute.String("component", "addition"),
	//)

	logger := logging.GetLogger(ctx)
	name := "Viraj"
	logger.Infof("[helloService :: GetName] Returning name %s", name)

	return name
}
