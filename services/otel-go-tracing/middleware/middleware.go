package middleware

import (
	"go.opentelemetry.io/contrib/instrumentation/net/http/otelhttp"
	"net/http"
)

func OtelMonitoring(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		wrappedHandler := otelhttp.NewHandler(next, r.RequestURI)
		wrappedHandler.ServeHTTP(w, r)

	})
}
