package com.example.otelspringtracing.manager;

import com.example.otelspringtracing.service.GreetingService;
import io.opentelemetry.api.trace.Span;
import io.opentelemetry.instrumentation.annotations.WithSpan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class GreetingManager {

    @Autowired
    private GreetingService greetingService;

    @WithSpan
    public String getGreetingsFromGo() {
//        OpenTelemetry.getGlobalTracer("com.lightstep.examples.server.ApiContextHandler
        Span span = Span.current();
        span.setAttribute("custom-key", "custom-value");

        return greetingService.getGreetingsFromGo();
    }

}
