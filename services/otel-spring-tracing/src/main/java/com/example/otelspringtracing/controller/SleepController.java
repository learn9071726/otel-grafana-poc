package com.example.otelspringtracing.controller;

import com.example.otelspringtracing.service.SleepService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/otel-tracing/sleep")
public class SleepController {

    @Autowired
    SleepService sleepService;

    @GetMapping("/1")
    public String sleepOne() throws InterruptedException {
        return sleepService.sleepOne();
    }

    @GetMapping("/2")
    public String sleepTwo() throws InterruptedException {
        return sleepService.sleepTwo();
    }

    @GetMapping("/3")
    public String sleepThree() throws InterruptedException {
        return sleepService.sleepThree();
    }

    @GetMapping("/4")
    public String sleepFour() throws InterruptedException {
        return sleepService.sleepFour();
    }

    @GetMapping("/5")
    public String sleepFive() throws InterruptedException {
        return sleepService.sleepFive();
    }

}
