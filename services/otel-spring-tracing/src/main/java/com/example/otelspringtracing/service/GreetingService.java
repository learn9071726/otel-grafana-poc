package com.example.otelspringtracing.service;

import io.opentelemetry.instrumentation.annotations.WithSpan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class GreetingService {

    private final Logger logger = LoggerFactory.getLogger(GreetingService.class);

    @Autowired
    private RestTemplate restTemplate;

    @WithSpan
    public String getGreetingsFromGo() {
        logger.info("Starting to call GoLang Service !!");
        String res = restTemplate.getForObject("http://go-app:8080/hello-from-go", String.class);
        logger.info("Received greetings from GoLang Service {}", res);
        return res;
    }

}

