package com.example.otelspringtracing.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class SleepService {

    private final Logger logger = LoggerFactory.getLogger(SleepService.class);

    public String sleepOne() throws InterruptedException {
        Thread.sleep(1000);
        logger.info("slept for one second");
        return "slept for one second";
    }

    public String sleepTwo() throws InterruptedException {
        Thread.sleep(2000);
        logger.info("slept for two seconds");
        return "slept for two seconds";
    }

    public String sleepThree() throws InterruptedException {
        Thread.sleep(3000);
        logger.info("slept for three seconds");
        return "slept for three seconds";
    }

    public String sleepFour() throws InterruptedException {
        Thread.sleep(4000);
        logger.info("slept for four seconds");
        return "slept for four seconds";
    }

    public String sleepFive() throws InterruptedException {
        Thread.sleep(5000);
        logger.info("slept for five seconds");
        return "slept for five seconds";
    }


}
