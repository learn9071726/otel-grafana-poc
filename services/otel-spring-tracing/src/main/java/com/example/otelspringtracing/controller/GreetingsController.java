package com.example.otelspringtracing.controller;

import com.example.otelspringtracing.manager.GreetingManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/otel-tracing")
public class GreetingsController {

    private final Logger logger = LoggerFactory.getLogger(GreetingsController.class);

    @Autowired
    private GreetingManager greetingManager;

//    @Autowired
//    private OpenTelemetry openTelemetry;

    @GetMapping("/hi")
    public String helloFromService2() {
        logger.info("Hello Otel");
        logger.error("error log from Otel");
        return "Hello Otel";
    }

    @GetMapping("/hi-go")
    public String talkToGoService() {
        return greetingManager.getGreetingsFromGo();
    }

//    update_account_details_api_url: api/1/bss/customer/updateallcustomeraccountsdetails
//    update_account_details_api_name : Update Account Details API
//    update_billing_address_api_url: api/2/bss/customer/updatebillingaccount
//    update_billing_address_api_name: Update Billing Address API
//    update_dob_api_url: api/1/bss/customer/updatecustomerdob


    @PostMapping("api/1/bss/customer/updateallcustomeraccountsdetails")
    public ResponseEntity updateCustomerAccDetails(@RequestBody Object data) {
        return ResponseEntity.internalServerError().build();
    }

    @PostMapping("api/2/bss/customer/updatebillingaccount")
    public ResponseEntity updateCustomerAddressDetails(@RequestBody Object data) {
        return ResponseEntity.internalServerError().build();
    }

    @PostMapping("api/1/bss/customer/updatecustomerdob")
    public ResponseEntity updateCustomerDobDetails(@RequestBody Object data) {
        return ResponseEntity.internalServerError().build();
    }


}