#!/bin/bash

export OTEL_EXPORTER_OTLP_ENDPOINT=http://localhost:5555
export OTEL_LOGS_EXPORTER=otlp
export OTEL_TRACES_EXPORTER=otlp
export OTEL_METRICS_EXPORTER=otlp
export OTEL_SERVICE_NAME=otel-spring-vg-tracing
export OTEL_RESOURCE_ATTRIBUTES=service.name=otel-spring-vg-tracing
export OTEL_TRACES_SAMPLER=always_on


java -javaagent:./opentelemetry-javaagent-1.22.0.jar \
     -jar target/otel-spring-tracing-0.0.1-SNAPSHOT.jar \
     --spring.config.location=./src/main/resources/application.properties


#java -jar target/spring-tracing-0.0.1-SNAPSHOT.jar \
#     --spring.config.location=./src/main/resources/application.proper